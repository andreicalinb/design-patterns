<?php declare(strict_types=1);

namespace More\Repository\Ferret;

class Ferret
{
    public static function fromData(int $id, string $name, float $weight): Ferret
    {
        return new self($id, $name, $weight);
    }

    public static function fromState(array $state): Ferret
    {
        return new self($state['id'], $state['name'], $state['weight']);
    }

    private function __construct(
        private int $id,
        private string $name,
        private float $weight
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }
}
