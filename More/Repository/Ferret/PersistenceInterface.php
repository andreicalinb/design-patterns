<?php declare(strict_types=1);

namespace More\Repository\Ferret;

interface PersistenceInterface
{
    public function generateId(): int;

    public function persist(array $data): void;

    public function retrieve(int $id): array;

    public function delete(int $id): void;
}
