<?php declare(strict_types=1);

namespace More\Repository\Ferret;

use OutOfBoundsException;

class InMemoryPersistence implements PersistenceInterface
{
    private array $data = [];
    private int $lastId = 0;

    public function generateId(): int
    {
        $this->lastId++;

        return $this->lastId;
    }

    public function persist(array $data): void
    {
        $this->data[$this->lastId] = $data;
    }

    public function retrieve(int $id): array
    {
        if (!isset($this->data[$id])) {
            throw new OutOfBoundsException(sprintf('No entity with id %d found', $id));
        }

        return $this->data[$id];
    }

    public function delete(int $id): void
    {
        if (!isset($this->data[$id])) {
            throw new OutOfBoundsException(sprintf('No entity with id %d found', $id));
        }

        unset($this->data[$id]);
    }
}
