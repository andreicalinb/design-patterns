<?php declare(strict_types=1);

namespace More\Repository\Ferret;

use OutOfBoundsException;

class FerretRepository
{
    public function __construct(private PersistenceInterface $persistence)
    {
    }

    public function generateId(): int
    {
        return $this->persistence->generateId();
    }

    public function findById(int $id): Ferret
    {
        try {
            $arrayData = $this->persistence->retrieve($id);

            return Ferret::fromState($arrayData);
        } catch (OutOfBoundsException $e) {
            throw new OutOfBoundsException(sprintf('Ferret with id %d does not exist', $id), 0, $e);
        }
    }

    public function save(Ferret $ferret): void
    {
        $this->persistence->persist(
            ['id' => $ferret->getId(), 'name' => $ferret->getName(), 'weight' => $ferret->getWeight()]
        );
    }
}
