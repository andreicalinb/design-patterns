<?php declare(strict_types=1);

namespace More\Repository;

use More\Repository\Ferret\Ferret;
use More\Repository\Ferret\FerretRepository;
use More\Repository\Ferret\InMemoryPersistence;
use OutOfBoundsException;
use TestCase;

class RepositoryTest extends TestCase
{
    public function test(): void
    {
        $repository = new FerretRepository(new InMemoryPersistence());
        $this->assertSame(1, $repository->generateId());

        try {
            $repository->findById(13);
            echo 'You should not see me';
        } catch (OutOfBoundsException $ex) {
        }

        $ferretId = $repository->generateId();
        $ferret = Ferret::fromData($ferretId, 'McFerry', 0.76);
        $repository->save($ferret);
        $foundFerret = $repository->findById($ferretId);
        $this->assertSame($ferretId, $foundFerret->getId());
        $this->assertSame('McFerry', $foundFerret->getName());
        $this->assertSame(0.76, $foundFerret->getWeight());
    }
}
