<?php declare(strict_types=1);

namespace More\ServiceLocator\Service;

class SomeService implements ServiceInterface
{
    public function stuff(): void
    {
    }
}
