<?php

namespace More\ServiceLocator\Service;

interface ServiceInterface
{
    public function stuff(): void;
}
