<?php declare(strict_types=1);

namespace More\ServiceLocator;

use More\ServiceLocator\Service\ServiceLocator;
use More\ServiceLocator\Service\SomeService;
use TestCase;

class ServiceLocatorTest extends TestCase
{
    public function test(): void
    {
        $serviceLocator = new ServiceLocator();
        $someService = new SomeService();
        $serviceLocator->addInstance(SomeService::class, $someService);
        $this->assertTrue($serviceLocator->has(SomeService::class));
        $this->assertTrue(!$serviceLocator->has(self::class));
        $this->assertClass(SomeService::class, $someService);
    }
}
