<?php declare(strict_types=1);

namespace More\EAV;

use More\EAV\Entity\Attribute;
use More\EAV\Entity\Entity;
use More\EAV\Entity\Value;
use TestCase;

class EAVTest extends TestCase
{
    public function test(): void
    {
        $sizeAttribute = new Attribute('size');
        $sizeS = new Value($sizeAttribute, 'S');
        $sizeM = new Value($sizeAttribute, 'M');
        $sizeL = new Value($sizeAttribute, 'L');

        $materialAttribute = new Attribute('material');
        $materialCotton = new Value($materialAttribute, 'cotton');

        $entity = new Entity('Shirt', [$sizeS, $sizeM, $sizeL, $materialCotton]);
        $this->assertSame('Shirt, size: S, size: M, size: L, material: cotton', (string) $entity);
    }
}
