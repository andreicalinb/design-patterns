<?php

namespace Creational\AbstractFactory\Smartphone;

class MoreExpensiveIphoneSmartphone implements SmartphoneInterface
{
    public function doSmartphoneThing(): string
    {
        return "Iphoooneee $$$\n";
    }
}
