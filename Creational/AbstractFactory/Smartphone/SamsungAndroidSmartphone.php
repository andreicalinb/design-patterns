<?php

namespace Creational\AbstractFactory\Smartphone;

class SamsungAndroidSmartphone implements SmartphoneInterface
{
    public function doSmartphoneThing(): string
    {
        return "Annndroooiddd Samsung\n";
    }
}
