<?php

namespace Creational\AbstractFactory\Smartphone;

interface SmartphoneInterface
{
    public function doSmartphoneThing(): string;
}
