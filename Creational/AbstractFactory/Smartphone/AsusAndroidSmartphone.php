<?php

namespace Creational\AbstractFactory\Smartphone;

class AsusAndroidSmartphone implements SmartphoneInterface
{
    public function doSmartphoneThing(): string
    {
        return "Annndroooiddd Asus\n";
    }
}
