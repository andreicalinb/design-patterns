<?php

namespace Creational\AbstractFactory\Smartphone;

class IphoneSmartphone implements SmartphoneInterface
{
    public function doSmartphoneThing(): string
    {
        return "Iphoooneee\n";
    }
}
