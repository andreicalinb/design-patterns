<?php declare(strict_types=1);

namespace Creational\AbstractFactory;

use Creational\AbstractFactory\Factory\AndroidSmartphoneFactory;
use Creational\AbstractFactory\Factory\IphoneSmartphoneFactory;
use Creational\AbstractFactory\Factory\SmartphoneFactory;
use Creational\AbstractFactory\Smartphone\AsusAndroidSmartphone;
use Creational\AbstractFactory\Smartphone\IphoneSmartphone;
use Creational\AbstractFactory\Smartphone\MoreExpensiveIphoneSmartphone;
use Creational\AbstractFactory\Smartphone\SamsungAndroidSmartphone;
use Creational\AbstractFactory\Smartphone\SmartphoneInterface;
use TestCase;

class AbstractFactoryTest extends TestCase
{
    public function test(): void
    {
        $androidSmartphoneFactory = new AndroidSmartphoneFactory();
        $this->assertClass(SmartphoneFactory::class, $androidSmartphoneFactory);
        $androidNewSmartphone = $androidSmartphoneFactory->createNewSmartphone();
        $this->assertClass(SmartphoneInterface::class, $androidNewSmartphone);
        $this->assertClass(AsusAndroidSmartphone::class, $androidNewSmartphone);
        $androidOldSmartphone = $androidSmartphoneFactory->createOldSmartphone();
        $this->assertClass(SmartphoneInterface::class, $androidOldSmartphone);
        $this->assertClass(SamsungAndroidSmartphone::class, $androidOldSmartphone);

        $iphoneSmartphoneFactory = new IphoneSmartphoneFactory();
        $this->assertClass(SmartphoneFactory::class, $iphoneSmartphoneFactory);
        $androidNewSmartphone = $iphoneSmartphoneFactory->createNewSmartphone();
        $this->assertClass(SmartphoneInterface::class, $androidNewSmartphone);
        $this->assertClass(MoreExpensiveIphoneSmartphone::class, $androidNewSmartphone);
        $androidOldSmartphone = $iphoneSmartphoneFactory->createOldSmartphone();
        $this->assertClass(SmartphoneInterface::class, $androidOldSmartphone);
        $this->assertClass(IphoneSmartphone::class, $androidOldSmartphone);
    }
}
