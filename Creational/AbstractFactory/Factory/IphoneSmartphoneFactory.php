<?php

namespace Creational\AbstractFactory\Factory;

use Creational\AbstractFactory\Smartphone\IphoneSmartphone;
use Creational\AbstractFactory\Smartphone\MoreExpensiveIphoneSmartphone;
use Creational\AbstractFactory\Smartphone\SmartphoneInterface;

class IphoneSmartphoneFactory implements SmartphoneFactory
{
    public function createNewSmartphone(): SmartphoneInterface
    {
        return new MoreExpensiveIphoneSmartphone();
    }

    public function createOldSmartphone(): SmartphoneInterface
    {
        return new IphoneSmartphone();
    }
}
