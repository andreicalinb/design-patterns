<?php

namespace Creational\AbstractFactory\Factory;

use Creational\AbstractFactory\Smartphone\AsusAndroidSmartphone;
use Creational\AbstractFactory\Smartphone\SamsungAndroidSmartphone;
use Creational\AbstractFactory\Smartphone\SmartphoneInterface;

class AndroidSmartphoneFactory implements SmartphoneFactory
{
    public function createNewSmartphone(): SmartphoneInterface
    {
        return new AsusAndroidSmartphone();
    }

    public function createOldSmartphone(): SmartphoneInterface
    {
        return new SamsungAndroidSmartphone();
    }
}
