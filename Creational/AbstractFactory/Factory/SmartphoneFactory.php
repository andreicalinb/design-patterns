<?php

namespace Creational\AbstractFactory\Factory;

use Creational\AbstractFactory\Smartphone\SmartphoneInterface;

interface SmartphoneFactory
{
    public function createNewSmartphone(): SmartphoneInterface;

    public function createOldSmartphone(): SmartphoneInterface;
}
