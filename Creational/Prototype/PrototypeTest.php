<?php declare(strict_types=1);

namespace Creational\Prototype;

use Creational\Prototype\Smartphone\AndroidSmartphonePrototype;
use Creational\Prototype\Smartphone\IphoneSmartphonePrototype;
use TestCase;

class PrototypeTest extends TestCase
{
    public function test(): void
    {
        $androidSmartphonePrototype = new AndroidSmartphonePrototype();
        $iphoneSmartphonePrototype = new IphoneSmartphonePrototype();

        $clonedAndroidSmartphonePrototype = clone $androidSmartphonePrototype;
        $clonedAndroidSmartphonePrototype->setTitle('Stuff');
        $this->assertClass(AndroidSmartphonePrototype::class, $clonedAndroidSmartphonePrototype);

        $clonedIphoneSmartphonePrototype = clone $iphoneSmartphonePrototype;
        $clonedIphoneSmartphonePrototype->setTitle('Expensive stuff');
        $this->assertClass(IphoneSmartphonePrototype::class, $clonedIphoneSmartphonePrototype);
    }
}
