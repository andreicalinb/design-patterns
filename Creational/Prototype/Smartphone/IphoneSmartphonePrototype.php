<?php declare(strict_types=1);

namespace Creational\Prototype\Smartphone;

class IphoneSmartphonePrototype extends SmartphonePrototype
{
    protected string $type = 'Iphone';

    public function __clone(): void
    {
    }
}
