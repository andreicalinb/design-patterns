<?php declare(strict_types=1);

namespace Creational\Prototype\Smartphone;

abstract class SmartphonePrototype
{
    protected string $title;
    protected string $type;

    abstract public function __clone(): void;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
