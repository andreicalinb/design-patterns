<?php declare(strict_types=1);

namespace Creational\Prototype\Smartphone;

class AndroidSmartphonePrototype extends SmartphonePrototype
{
    protected string $type = 'Android';

    public function __clone(): void
    {
    }
}
