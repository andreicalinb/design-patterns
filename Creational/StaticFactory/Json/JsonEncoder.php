<?php declare(strict_types=1);

namespace Creational\StaticFactory\Json;

class JsonEncoder implements Formatter
{
    public function format(string $input): string
    {
        return json_encode($input);
    }
}
