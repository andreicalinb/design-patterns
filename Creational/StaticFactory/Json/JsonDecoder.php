<?php declare(strict_types=1);

namespace Creational\StaticFactory\Json;

class JsonDecoder implements Formatter
{
    public function format(string $input): string
    {
        return (string)json_decode($input);
    }
}
