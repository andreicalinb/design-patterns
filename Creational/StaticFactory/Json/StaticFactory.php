<?php declare(strict_types=1);

namespace Creational\StaticFactory\Json;

use InvalidArgumentException;

final class StaticFactory
{
    public static function factory(string $type): Formatter
    {
        if ($type === 'json_encode') {
            return new JsonEncoder();
        } elseif ($type === 'json_decode') {
            return new JsonDecoder();
        }

        throw new InvalidArgumentException('Unknown format given');
    }
}
