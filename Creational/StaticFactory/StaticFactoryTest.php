<?php declare(strict_types=1);

namespace Creational\StaticFactory;

use Creational\StaticFactory\Json\Formatter;
use Creational\StaticFactory\Json\JsonDecoder;
use Creational\StaticFactory\Json\JsonEncoder;
use Creational\StaticFactory\Json\StaticFactory;
use InvalidArgumentException;
use TestCase;

class StaticFactoryTest extends TestCase
{
    public function test(): void
    {
        $jsonEncoder = StaticFactory::factory('json_encode');
        $this->assertClass(Formatter::class, $jsonEncoder);
        $this->assertClass(JsonEncoder::class, $jsonEncoder);

        $jsonDecoder = StaticFactory::factory('json_decode');
        $this->assertClass(Formatter::class, $jsonDecoder);
        $this->assertClass(JsonDecoder::class, $jsonDecoder);

        try {
            StaticFactory::factory('bla');
            echo 'You should not see me';
        } catch (InvalidArgumentException) {
        }
    }
}
