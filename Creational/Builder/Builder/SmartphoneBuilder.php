<?php declare(strict_types=1);

namespace Creational\Builder\Builder;

use Creational\Builder\Parts\OS;
use Creational\Builder\Parts\Touchscreen;
use Creational\Builder\Parts\Smartphone;
use Creational\Builder\Parts\Phone;

class SmartphoneBuilder implements BuilderInterface
{
    private Smartphone $smartphone;

    public function addTouchscreen(): void
    {
        $this->smartphone->setThing('touchscreen', new Touchscreen());
    }

    public function addOS(): void
    {
        $this->smartphone->setThing('os', new OS());
    }

    public function createPhone(): void
    {
        $this->smartphone = new Smartphone();
    }

    public function getPhone(): Phone
    {
        return $this->smartphone;
    }
}
