<?php declare(strict_types=1);

namespace Creational\Builder\Builder;

use Creational\Builder\Parts\Phone;

interface BuilderInterface
{
    public function createPhone(): void;

    public function addTouchscreen(): void;

    public function addOS(): void;

    public function getPhone(): Phone;
}
