<?php declare(strict_types=1);

namespace Creational\Builder\Builder;

use Creational\Builder\Parts\OS;
use Creational\Builder\Parts\Touchscreen;
use Creational\Builder\Parts\AndroidSmartphone;
use Creational\Builder\Parts\Phone;

class AndroidSmartphoneBuilder implements BuilderInterface
{
    private AndroidSmartphone $androidSmartphone;

    public function addTouchscreen(): void
    {
        $this->androidSmartphone->setThing('touchscreen', new Touchscreen());
    }

    public function addOS(): void
    {
        $this->androidSmartphone->setThing('os', new OS());
    }

    public function createPhone(): void
    {
        $this->androidSmartphone = new AndroidSmartphone();
    }

    public function getPhone(): Phone
    {
        return $this->androidSmartphone;
    }
}
