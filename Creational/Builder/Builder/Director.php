<?php declare(strict_types=1);

namespace Creational\Builder\Builder;

use Creational\Builder\Parts\Phone;

class Director
{
    public function build(BuilderInterface $builder): Phone
    {
        $builder->createPhone();
        $builder->addTouchscreen();
        $builder->addOS();

        return $builder->getPhone();
    }
}
