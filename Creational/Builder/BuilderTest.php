<?php declare(strict_types=1);

namespace Creational\Builder;

use Creational\Builder\Builder\AndroidSmartphoneBuilder;
use Creational\Builder\Builder\BuilderInterface;
use Creational\Builder\Builder\Director;
use Creational\Builder\Builder\SmartphoneBuilder;
use Creational\Builder\Parts\AndroidSmartphone;
use Creational\Builder\Parts\Phone;
use Creational\Builder\Parts\Smartphone;
use TestCase;

class BuilderTest extends TestCase
{
    public function test(): void
    {
        $director = new Director();

        $smartphoneBuilder = new SmartphoneBuilder();
        $this->assertClass(BuilderInterface::class, $smartphoneBuilder);
        $smartphone = $director->build($smartphoneBuilder);
        $this->assertClass(Phone::class, $smartphone);
        $this->assertClass(Smartphone::class, $smartphone);

        $androidSmartphoneBuilder = new AndroidSmartphoneBuilder();
        $this->assertClass(BuilderInterface::class, $androidSmartphoneBuilder);
        $androidSmartphone = $director->build($androidSmartphoneBuilder);
        $this->assertClass(Phone::class, $androidSmartphone);
        $this->assertClass(AndroidSmartphone::class, $androidSmartphone);
    }
}
