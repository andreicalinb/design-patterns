<?php declare(strict_types=1);

namespace Creational\Singleton\Spotify;

use Exception;

final class Spotify
{
    private static ?Spotify $instance = null;

    /**
     * Lazy initialization
     */
    public static function getInstance(): Spotify
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * No constructor in order to not create multiple instances
     */
    private function __construct()
    {
    }

    /**
     * Don't let the object get cloned in order to not create multiple instances
     */
    private function __clone(): void
    {
    }
    
    /**
     * Don't let the object get unserialized  in order to not create multiple instances
     */
    public function __wakeup(): void
    {
        throw new Exception("Cannot unserialize a singleton class");
    }
}
