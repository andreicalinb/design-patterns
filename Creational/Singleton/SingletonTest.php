<?php declare(strict_types=1);

namespace Creational\Singleton;

use Creational\Singleton\Spotify\Spotify;
use TestCase;

class SingletonTest extends TestCase
{
    public function test(): void
    {
        $firstInstance = Spotify::getInstance();
        $secondInstance = Spotify::getInstance();
        $this->assertClass(Spotify::class, $firstInstance);
        $this->assertSame($firstInstance, $secondInstance);
    }
}
