<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Printer;

interface PrinterInterface
{
    public function print(string $message): void;
}
