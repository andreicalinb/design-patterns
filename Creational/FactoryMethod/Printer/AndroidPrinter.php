<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Printer;

class AndroidPrinter implements PrinterInterface
{
    public function print(string $message): void
    {
        file_put_contents('/tmp/null', $message, FILE_APPEND);
    }
}
