<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Printer;

class IphonePrinter implements PrinterInterface
{
    public function print(string $message): void
    {
        file_put_contents('/dev/null', $message . PHP_EOL, FILE_APPEND);
    }
}
