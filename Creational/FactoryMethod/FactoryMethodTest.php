<?php declare(strict_types=1);

namespace Creational\FactoryMethod;

use Creational\FactoryMethod\Factory\AndroidPrinterFactory;
use Creational\FactoryMethod\Factory\IphonePrinterFactory;
use Creational\FactoryMethod\Factory\PrinterFactoryInterface;
use Creational\FactoryMethod\Printer\AndroidPrinter;
use Creational\FactoryMethod\Printer\IphonePrinter;
use Creational\FactoryMethod\Printer\PrinterInterface;
use TestCase;

class FactoryMethodTest extends TestCase
{
    public function test(): void
    {
        $androidPrinterFactory = new AndroidPrinterFactory();
        $this->assertClass(PrinterFactoryInterface::class, $androidPrinterFactory);
        $androidPrinter = $androidPrinterFactory->createPrinter();
        $this->assertClass(PrinterInterface::class, $androidPrinter);
        $this->assertClass(AndroidPrinter::class, $androidPrinter);

        $iphonePrinterFactory = new IphonePrinterFactory();
        $this->assertClass(PrinterFactoryInterface::class, $iphonePrinterFactory);
        $iphonePrinter = $iphonePrinterFactory->createPrinter();
        $this->assertClass(PrinterInterface::class, $iphonePrinter);
        $this->assertClass(IphonePrinter::class, $iphonePrinter);
    }
}
