<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Factory;

use Creational\FactoryMethod\Printer\AndroidPrinter;
use Creational\FactoryMethod\Printer\PrinterInterface;

class AndroidPrinterFactory implements PrinterFactoryInterface
{
    public function createPrinter(): PrinterInterface
    {
        return new AndroidPrinter();
    }
}
