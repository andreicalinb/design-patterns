<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Factory;

use Creational\FactoryMethod\Printer\PrinterInterface;

interface PrinterFactoryInterface
{
    public function createPrinter(): PrinterInterface;
}
