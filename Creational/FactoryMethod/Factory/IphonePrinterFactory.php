<?php declare(strict_types=1);

namespace Creational\FactoryMethod\Factory;

use Creational\FactoryMethod\Printer\IphonePrinter;
use Creational\FactoryMethod\Printer\PrinterInterface;

class IphonePrinterFactory implements PrinterFactoryInterface
{
    public function createPrinter(): PrinterInterface
    {
        return new IphonePrinter();
    }
}
