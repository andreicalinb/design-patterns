<?php declare(strict_types=1);

namespace Creational\Pool\Worker;

use Countable;

class WorkerPool implements Countable
{
    /**
     * @var JsonEncodeWorker[]
     */
    private array $occupiedWorkers = [];

    /**
     * @var JsonEncodeWorker[]
     */
    private array $freeWorkers = [];

    public function getWorker(): JsonEncodeWorker
    {
        $worker = count($this->freeWorkers) === 0 ? new JsonEncodeWorker() : array_pop($this->freeWorkers);
        $this->occupiedWorkers[spl_object_hash($worker)] = $worker;

        return $worker;
    }

    public function disposeWorker(JsonEncodeWorker $worker): void
    {
        $key = spl_object_hash($worker);

        if (isset($this->occupiedWorkers[$key])) {
            unset($this->occupiedWorkers[$key]);
            $this->freeWorkers[$key] = $worker;
        }
    }

    public function count(): int
    {
        return count($this->occupiedWorkers) + count($this->freeWorkers);
    }
}
