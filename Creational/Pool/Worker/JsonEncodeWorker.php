<?php declare(strict_types=1);

namespace Creational\Pool\Worker;

class JsonEncodeWorker
{
    public function work(string $text): string
    {
        return json_encode($text);
    }
}
