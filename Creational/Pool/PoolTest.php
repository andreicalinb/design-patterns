<?php declare(strict_types=1);

namespace Creational\Pool;

use Creational\Pool\Worker\WorkerPool;
use TestCase;

class PoolTest extends TestCase
{
    public function test(): void
    {
        $pool = new WorkerPool();
        $worker1 = $pool->getWorker();
        $worker2 = $pool->getWorker();
        $this->assertSame(2, $pool->count());
        $this->assertNotSame($worker1, $worker2);

        $pool->disposeWorker($worker1);
        $this->assertSame(2, $pool->count());
        $worker3 = $pool->getWorker();
        $this->assertSame($worker1, $worker3);
    }
}
