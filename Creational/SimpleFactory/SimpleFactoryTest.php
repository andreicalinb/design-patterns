<?php declare(strict_types=1);

namespace Creational\SimpleFactory;

use Creational\SimpleFactory\Skype\Skype;
use Creational\SimpleFactory\Skype\SkypeFactory;
use TestCase;

class SimpleFactoryTest extends TestCase
{
    public function test(): void
    {
        $skypeFactory = new SkypeFactory();
        $skype = $skypeFactory->createSkype();
        $this->assertClass(Skype::class, $skype);
    }
}
