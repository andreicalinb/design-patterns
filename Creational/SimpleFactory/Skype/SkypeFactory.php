<?php declare(strict_types=1);

namespace Creational\SimpleFactory\Skype;

class SkypeFactory
{
    public function createSkype(): Skype
    {
        return new Skype();
    }
}
