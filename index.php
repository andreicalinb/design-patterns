<?php

use Behavioral\ChainOfResponsibilities\ChainOfResponsibilitiesTest;
use Behavioral\Command\CommandTest;
use Behavioral\Interpreter\InterpreterTest;
use Behavioral\Iterator\IteratorTest;
use Behavioral\Mediator\MediatorTest;
use Behavioral\Memento\MementoTest;
use Behavioral\NullObject\NullObjectTest;
use Behavioral\Observer\ObserverTest;
use Behavioral\Specification\SpecificationTest;
use Behavioral\State\StateTest;
use Behavioral\Strategy\StrategyTest;
use Behavioral\TemplateMethod\TemplateMethodTest;
use Behavioral\Visitor\VisitorTest;
use Creational\AbstractFactory\AbstractFactoryTest;
use Creational\Builder\BuilderTest;
use Creational\FactoryMethod\FactoryMethodTest;
use Creational\Pool\PoolTest;
use Creational\Prototype\PrototypeTest;
use Creational\SimpleFactory\SimpleFactoryTest;
use Creational\Singleton\SingletonTest;
use Creational\StaticFactory\StaticFactoryTest;
use More\EAV\EAVTest;
use More\Repository\RepositoryTest;
use More\ServiceLocator\ServiceLocatorTest;
use Structural\Adapter\AdapterTest;
use Structural\Bridge\BridgeTest;
use Structural\Composite\CompositeTest;
use Structural\DataMapper\DataMapperTest;
use Structural\Decorator\DecoratorTest;
use Structural\DependencyInjection\DependencyInjectionTest;
use Structural\Facade\FacadeTest;
use Structural\FluentInterface\FluentInterfaceTest;
use Structural\Flyweight\FlyweightTest;
use Structural\Proxy\ProxyTest;
use Structural\Registry\RegistryTest;

include 'autoload.php';

$designPatterns = [
    AbstractFactoryTest::class,
    BuilderTest::class,
    FactoryMethodTest::class,
    PrototypeTest::class,
    SingletonTest::class,
    AdapterTest::class,
    BridgeTest::class,
    CompositeTest::class,
    DecoratorTest::class,
    FacadeTest::class,
    FlyweightTest::class,
    ProxyTest::class,
    ChainOfResponsibilitiesTest::class,
    CommandTest::class,
    InterpreterTest::class,
    IteratorTest::class,
    MediatorTest::class,
    MementoTest::class,
    ObserverTest::class,
    StateTest::class,
    StrategyTest::class,
    TemplateMethodTest::class,
    VisitorTest::class,
    NullObjectTest::class,
    SimpleFactoryTest::class,
    PoolTest::class,
    RegistryTest::class,
    DependencyInjectionTest::class,
    FluentInterfaceTest::class,
    DataMapperTest::class,
    StaticFactoryTest::class,
    SpecificationTest::class,
    ServiceLocatorTest::class,
    RepositoryTest::class,
    EAVTest::class,
];
foreach ($designPatterns as $designPattern) {
    (new $designPattern)->test();
}