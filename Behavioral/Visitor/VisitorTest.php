<?php declare(strict_types=1);

namespace Behavioral\Visitor;

use Behavioral\Visitor\Shopping\BancaTransilvania;
use Behavioral\Visitor\Shopping\Lidl;
use Behavioral\Visitor\Shopping\RecordingVisitor;
use TestCase;

class VisitorTest extends TestCase
{
    public function test(): void
    {
        $visitor = new RecordingVisitor();

        $lidl = new Lidl('Frunzisului');
        $lidl->accept($visitor);
        $this->assertSame($lidl, $visitor->getVisited()[0]);

        $bancaTransilvania = new BancaTransilvania('Minerva');
        $bancaTransilvania->accept($visitor);
        $this->assertSame($bancaTransilvania, $visitor->getVisited()[1]);
    }
}
