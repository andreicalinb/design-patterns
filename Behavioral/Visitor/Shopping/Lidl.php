<?php declare(strict_types=1);

namespace Behavioral\Visitor\Shopping;

class Lidl implements RoleInterface
{
    public function __construct(private string $name)
    {
    }

    public function getName(): string
    {
        return sprintf('Lidl: %s', $this->name);
    }

    public function accept(RoleVisitorInterface $visitor): void
    {
        $visitor->visitLidl($this);
    }
}
