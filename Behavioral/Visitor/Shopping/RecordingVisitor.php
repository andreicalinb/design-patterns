<?php declare(strict_types=1);

namespace Behavioral\Visitor\Shopping;

class RecordingVisitor implements RoleVisitorInterface
{
    /**
     * @var RoleInterface[]
     */
    private array $visited = [];

    public function visitLidl(Lidl $lidl): void
    {
        $this->visited[] = $lidl;
    }

    public function visitBancaTransilvania(BancaTransilvania $bancaTransilvania): void
    {
        $this->visited[] = $bancaTransilvania;
    }

    /**
     * @return RoleInterface[]
     */
    public function getVisited(): array
    {
        return $this->visited;
    }
}
