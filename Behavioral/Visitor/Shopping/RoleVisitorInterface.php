<?php declare(strict_types=1);

namespace Behavioral\Visitor\Shopping;

interface RoleVisitorInterface
{
    public function visitLidl(Lidl $lidl): void;

    public function visitBancaTransilvania(BancaTransilvania $bancaTransilvania): void;
}
