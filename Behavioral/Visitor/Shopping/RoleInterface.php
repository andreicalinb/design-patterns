<?php declare(strict_types=1);

namespace Behavioral\Visitor\Shopping;

interface RoleInterface
{
    public function accept(RoleVisitorInterface $visitor): void;
}
