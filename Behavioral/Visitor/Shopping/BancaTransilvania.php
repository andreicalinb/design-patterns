<?php declare(strict_types=1);

namespace Behavioral\Visitor\Shopping;

class BancaTransilvania implements RoleInterface
{
    public function __construct(private string $name)
    {
    }

    public function getName(): string
    {
        return sprintf('User %s', $this->name);
    }

    public function accept(RoleVisitorInterface $visitor): void
    {
        $visitor->visitBancaTransilvania($this);
    }
}
