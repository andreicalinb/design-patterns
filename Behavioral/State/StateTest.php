<?php declare(strict_types=1);

namespace Behavioral\State;

use Behavioral\State\Phone\Phone;
use TestCase;

class StateTest extends TestCase
{
    public function test(): void
    {
        $phone = Phone::create();
        $this->assertSame('Normal', $phone->toString());

        $phone->proceedToNext();
        $this->assertSame('Vibration', $phone->toString());

        $phone->proceedToNext();
        $this->assertSame('Silent', $phone->toString());

        $phone->proceedToNext();
        $this->assertSame('Normal', $phone->toString());
    }
}
