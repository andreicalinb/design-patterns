<?php declare(strict_types=1);

namespace Behavioral\State\Phone;

class Silent implements StateInterface
{
    public function proceedToNext(Phone $phone): void
    {
        $phone->setState(new Normal());
    }

    public function toString(): string
    {
        return 'Silent';
    }
}
