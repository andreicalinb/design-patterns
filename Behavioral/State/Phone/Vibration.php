<?php declare(strict_types=1);

namespace Behavioral\State\Phone;

class Vibration implements StateInterface
{
    public function proceedToNext(Phone $phone): void
    {
        $phone->setState(new Silent());
    }

    public function toString(): string
    {
        return 'Vibration';
    }
}
