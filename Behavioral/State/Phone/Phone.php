<?php declare(strict_types=1);

namespace Behavioral\State\Phone;

class Phone
{
    private StateInterface $state;

    public static function create(): Phone
    {
        $phone = new self();
        $phone->state = new Normal();

        return $phone;
    }

    public function setState(StateInterface $state): void
    {
        $this->state = $state;
    }

    public function proceedToNext(): void
    {
        $this->state->proceedToNext($this);
    }

    public function toString(): string
    {
        return $this->state->toString();
    }
}
