<?php declare(strict_types=1);

namespace Behavioral\State\Phone;

interface StateInterface
{
    public function proceedToNext(Phone $phone): void;

    public function toString(): string;
}
