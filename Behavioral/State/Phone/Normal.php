<?php declare(strict_types=1);

namespace Behavioral\State\Phone;

class Normal implements StateInterface
{
    public function proceedToNext(Phone $phone): void
    {
        $phone->setState(new Vibration());
    }

    public function toString(): string
    {
        return 'Normal';
    }
}
