<?php declare(strict_types=1);

namespace Behavioral\NullObject;

use Behavioral\NullObject\Cat\Cat;
use Behavioral\NullObject\Cat\CatService;
use Behavioral\NullObject\Cat\NullCat;
use TestCase;

class NullObjectTest extends TestCase
{
    public function test(): void
    {
        $catService = new CatService(new Cat());
        ob_start();
        $catService->makeTheCatPurr();
        $result = ob_get_contents();
        ob_end_clean();
        $this->assertSame('Purrr', $result);

        $nullCatService = new CatService(new NullCat());
        ob_start();
        $nullCatService->makeTheCatPurr();
        $result = ob_get_contents();
        ob_end_clean();
        $this->assertSame('', $result);
    }
}
