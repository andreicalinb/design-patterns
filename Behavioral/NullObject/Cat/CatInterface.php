<?php declare(strict_types=1);

namespace Behavioral\NullObject\Cat;

interface CatInterface
{
    public function purr(string $str): void;
}
