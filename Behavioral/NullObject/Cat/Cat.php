<?php declare(strict_types=1);

namespace Behavioral\NullObject\Cat;

class Cat implements CatInterface
{
    public function purr(string $str): void
    {
        echo $str;
    }
}
