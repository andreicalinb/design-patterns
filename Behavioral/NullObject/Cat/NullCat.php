<?php declare(strict_types=1);

namespace Behavioral\NullObject\Cat;

class NullCat implements CatInterface
{
    public function purr(string $str): void
    {
    }
}
