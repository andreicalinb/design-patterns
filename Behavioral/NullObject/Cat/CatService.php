<?php declare(strict_types=1);

namespace Behavioral\NullObject\Cat;

class CatService
{
    public function __construct(private CatInterface $cat)
    {
    }

    public function makeTheCatPurr(): void
    {
        $this->cat->purr('Purrr');
    }
}
