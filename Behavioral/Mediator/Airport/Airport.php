<?php declare(strict_types=1);

namespace Behavioral\Mediator\Airport;

class Airport extends Colleague
{
    public function getFlightInfo(string $flightName): string
    {
        return $this->mediator->getFlight($flightName);
    }
}
