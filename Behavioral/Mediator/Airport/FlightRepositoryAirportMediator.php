<?php declare(strict_types=1);

namespace Behavioral\Mediator\Airport;

class FlightRepositoryAirportMediator implements MediatorInterface
{
    public function __construct(private FlightRepository $flightRepository, private Airport $airport)
    {
        $this->flightRepository->setMediator($this);
        $this->airport->setMediator($this);
    }

    public function getInfoAbout(string $flightName): string
    {
        return $this->airport->getFlightInfo($flightName);
    }

    public function getFlight(string $flightName): string
    {
        return $this->flightRepository->getFlightName($flightName);
    }
}
