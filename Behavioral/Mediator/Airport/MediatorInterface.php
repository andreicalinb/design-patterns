<?php declare(strict_types=1);

namespace Behavioral\Mediator\Airport;

interface MediatorInterface
{
    public function getFlight(string $flightName): string;
}
