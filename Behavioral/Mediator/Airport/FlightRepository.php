<?php declare(strict_types=1);

namespace Behavioral\Mediator\Airport;

class FlightRepository extends Colleague
{
    public function getFlightName(string $flightName): string
    {
        return 'Flight: ' . $flightName;
    }
}
