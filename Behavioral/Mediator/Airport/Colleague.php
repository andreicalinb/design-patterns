<?php declare(strict_types=1);

namespace Behavioral\Mediator\Airport;

abstract class Colleague
{
    protected MediatorInterface $mediator;

    public function setMediator(MediatorInterface $mediator): void
    {
        $this->mediator = $mediator;
    }
}
