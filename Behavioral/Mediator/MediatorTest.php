<?php declare(strict_types=1);

namespace Behavioral\Mediator;

use Behavioral\Mediator\Airport\Airport;
use Behavioral\Mediator\Airport\FlightRepository;
use Behavioral\Mediator\Airport\FlightRepositoryAirportMediator;
use TestCase;

class MediatorTest extends TestCase
{
    public function test(): void
    {
        $mediator = new FlightRepositoryAirportMediator(new FlightRepository(), new Airport());
        $this->assertSame('Flight: Amsterdam', $mediator->getInfoAbout('Amsterdam'));
    }
}
