<?php declare(strict_types=1);

namespace Behavioral\Iterator;

use Behavioral\Iterator\Song\Song;
use Behavioral\Iterator\Song\SongList;
use TestCase;

class IteratorTest extends TestCase
{
    public function test(): void
    {
        $songList = new SongList();
        $firstSong = new Song('The Last Stand', 'Sabaton');
        $songList->addSong($firstSong);
        $secondSong = new Song('Sparta', 'Sabaton');
        $songList->addSong($secondSong);

        $songs = [];
        while ($songList->valid()) {
            $songs[] = $songList->current()->getArtistAndTitle();
            $songList->next();
        }
        $this->assertSame(['The Last Stand by Sabaton', 'Sparta by Sabaton'], $songs);
        $this->assertSame(2, $songList->count());

        $songList->removeSong($secondSong);
        $songList->rewind();
        $songs = [];
        while ($songList->valid()) {
            $songs[] = $songList->current()->getArtistAndTitle();
            $songList->next();
        }
        $this->assertSame(['The Last Stand by Sabaton'], $songs);
        $this->assertSame(1, $songList->count());

    }
}
