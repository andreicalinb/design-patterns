<?php declare(strict_types=1);

namespace Behavioral\Iterator\Song;

use Countable;
use Iterator;

class SongList implements Countable, Iterator
{
    /**
     * @var Song[]
     */
    private array $song = [];

    private int $currentIndex = 0;

    public function addSong(Song $song): void
    {
        $this->song[] = $song;
    }

    public function removeSong(Song $songToRemove): void
    {
        foreach ($this->song as $key => $song) {
            if ($song->getArtistAndTitle() === $songToRemove->getArtistAndTitle()) {
                unset($this->song[$key]);
                break;
            }
        }

        $this->song = array_values($this->song);
    }

    public function count(): int
    {
        return count($this->song);
    }

    public function current(): Song
    {
        return $this->song[$this->currentIndex];
    }

    public function key(): int
    {
        return $this->currentIndex;
    }

    public function next(): void
    {
        $this->currentIndex++;
    }

    public function rewind(): void
    {
        $this->currentIndex = 0;
    }

    public function valid(): bool
    {
        return isset($this->song[$this->currentIndex]);
    }
}
