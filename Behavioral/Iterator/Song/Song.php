<?php declare(strict_types=1);

namespace Behavioral\Iterator\Song;

class Song
{
    public function __construct(private string $title, private string $artist)
    {
    }

    public function getArtist(): string
    {
        return $this->artist;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getArtistAndTitle(): string
    {
        return $this->getTitle() . ' by ' . $this->getArtist();
    }
}
