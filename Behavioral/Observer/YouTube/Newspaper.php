<?php declare(strict_types=1);

namespace Behavioral\Observer\YouTube;

use SplSubject;
use SplObjectStorage;
use SplObserver;

class Newspaper implements SplSubject
{
    private SplObjectStorage $observers;

    private string $article;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function attach(SplObserver $observer): void
    {
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer): void
    {
        $this->observers->detach($observer);
    }

    public function addArticle(string $article): void
    {
        $this->article = $article;

        $this->notify();
    }

    public function getArticle(): string
    {
        return $this->article;
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            /** @var SplObserver $observer */
            $observer->update($this);
        }
    }
}
