<?php declare(strict_types=1);

namespace Behavioral\Observer\YouTube;

use SplObserver;
use SplSubject;

class NewspaperObserver implements SplObserver
{
    /**
     * @var string[]
     */
    private array $newArticles = [];

    public function update(SplSubject $subject): void
    {
        /** @var Newspaper $subject */
        $this->newArticles[] = $subject->getArticle();
    }

    /**
     * @return string[]
     */
    public function getNewArticles(): array
    {
        return $this->newArticles;
    }
}
