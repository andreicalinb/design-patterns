<?php declare(strict_types=1);

namespace Behavioral\Observer;

use Behavioral\Observer\YouTube\Newspaper;
use Behavioral\Observer\YouTube\NewspaperObserver;
use TestCase;

class ObserverTest extends TestCase
{
    public function test(): void
    {
        $newspaperObserver = new NewspaperObserver();

        $newspaper = new Newspaper();
        $newspaper->attach($newspaperObserver);
        $this->assertSame([], $newspaperObserver->getNewArticles());
        $newspaper->addArticle('Blaaaa');
        $this->assertSame(['Blaaaa'], $newspaperObserver->getNewArticles());
    }
}
