<?php declare(strict_types=1);

namespace Behavioral\Specification\Number;

interface SpecificationInterface
{
    public function isSatisfiedBy(NumberValue $number): bool;
}
