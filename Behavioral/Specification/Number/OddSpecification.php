<?php declare(strict_types=1);

namespace Behavioral\Specification\Number;

class OddSpecification implements SpecificationInterface
{
    public function isSatisfiedBy(NumberValue $number): bool
    {
        return $number->getNumber() % 2 === 1;
    }
}
