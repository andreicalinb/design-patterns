<?php declare(strict_types=1);

namespace Behavioral\Specification\Number;

class NumberValue
{
    public function __construct(private float $value)
    {
    }

    public function getNumber(): float
    {
        return $this->value;
    }
}
