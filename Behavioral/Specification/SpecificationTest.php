<?php declare(strict_types=1);

namespace Behavioral\Specification;

use Behavioral\Specification\Number\EvenSpecification;
use Behavioral\Specification\Number\NumberValue;
use Behavioral\Specification\Number\OddSpecification;
use TestCase;

class SpecificationTest extends TestCase
{
    public function test(): void
    {
        $evenSpecification = new EvenSpecification();
        $this->assertSame(false, $evenSpecification->isSatisfiedBy(new NumberValue(11)));
        $this->assertSame(true, $evenSpecification->isSatisfiedBy(new NumberValue(10)));

        $oddSpecification = new OddSpecification();
        $this->assertSame(true, $oddSpecification->isSatisfiedBy(new NumberValue(11)));
        $this->assertSame(false, $oddSpecification->isSatisfiedBy(new NumberValue(10)));
    }
}
