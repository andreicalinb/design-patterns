<?php declare(strict_types=1);

namespace Behavioral\ChainOfResponsibilities\Logger;

abstract class LoggerHandler
{
    public function __construct(private ?LoggerHandler $successor = null)
    {
    }

    final public function handle(string $message): ?string
    {
        $processed = $this->processing($message);

        if ($processed === null && $this->successor !== null) {
            $processed = $this->successor->handle($message);
        }

        return $processed;
    }

    abstract protected function processing(string $message): ?string;
}
