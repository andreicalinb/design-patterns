<?php declare(strict_types=1);

namespace Behavioral\ChainOfResponsibilities\Logger;

class FileLogger extends LoggerHandler
{
    protected function processing(string $message): ?string
    {
        return 'Hello file!';
    }
}
