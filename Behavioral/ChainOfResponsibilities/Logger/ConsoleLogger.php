<?php declare(strict_types=1);

namespace Behavioral\ChainOfResponsibilities\Logger;

class ConsoleLogger extends LoggerHandler
{
    protected function processing(string $message): ?string
    {
        if (str_contains($message, 'console')) {
            return $message;
        }

        return null;
    }
}
