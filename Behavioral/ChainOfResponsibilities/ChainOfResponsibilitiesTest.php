<?php declare(strict_types=1);

namespace Behavioral\ChainOfResponsibilities;

use Behavioral\ChainOfResponsibilities\Logger\ConsoleLogger;
use Behavioral\ChainOfResponsibilities\Logger\FileLogger;

class ChainOfResponsibilitiesTest extends \TestCase
{
    public function test(): void
    {
        $fileLogger = new FileLogger();
        $this->assertSame('Hello file!', $fileLogger->handle('stuff'));

        $consoleLogger = new ConsoleLogger($fileLogger);
        $this->assertSame('Hello file!', $consoleLogger->handle('stuff'));
        $this->assertSame('console', $consoleLogger->handle('console'));
    }
}
