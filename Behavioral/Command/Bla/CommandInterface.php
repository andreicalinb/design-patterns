<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

interface CommandInterface
{
    public function execute(): void;
}
