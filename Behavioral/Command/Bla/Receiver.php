<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

class Receiver
{
    /**
     * @var string[]
     */
    private array $output = [];

    public function write(string $str): void
    {
        $this->output[] = $str;
    }

    public function getOutput(): string
    {
        return join("\n", $this->output);
    }

    public function clearOutput(): void
    {
        $this->output = [];
    }
}
