<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

class BlaBlaBlaCommand implements UndoableCommandInterface
{
    public function __construct(private Receiver $output)
    {
    }

    public function execute(): void
    {
        $this->output->write('Bla bla bla');
    }

    public function undo(): void
    {
        $this->output->clearOutput();
    }
}
