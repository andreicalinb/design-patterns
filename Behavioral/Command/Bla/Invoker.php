<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

class Invoker
{
    private UndoableCommandInterface $command;

    public function setCommand(UndoableCommandInterface $command): void
    {
        $this->command = $command;
    }

    public function run(): void
    {
        $this->command->execute();
    }

    public function undo(): void
    {
        $this->command->undo();
    }
}
