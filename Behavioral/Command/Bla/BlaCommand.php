<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

class BlaCommand implements CommandInterface
{
    public function __construct(private Receiver $output)
    {
    }

    public function execute(): void
    {
        $this->output->write('Bla');
    }
}
