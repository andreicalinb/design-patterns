<?php declare(strict_types=1);

namespace Behavioral\Command\Bla;

interface UndoableCommandInterface extends CommandInterface
{
    public function undo(): void;
}
