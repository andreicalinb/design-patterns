<?php declare(strict_types=1);

namespace Behavioral\Command;

use Behavioral\Command\Bla\BlaBlaBlaCommand;
use Behavioral\Command\Bla\BlaCommand;
use Behavioral\Command\Bla\Invoker;
use Behavioral\Command\Bla\Receiver;

class CommandTest extends \TestCase
{
    public function test(): void
    {
        $receiver = new Receiver();
        $blaCommand = new BlaCommand($receiver);
        $blaCommand->execute();
        $this->assertSame('Bla', $receiver->getOutput());

        $invoker = new Invoker();
        $receiver = new Receiver();
        $invoker->setCommand(new BlaBlaBlaCommand($receiver));
        $invoker->run();
        $this->assertSame('Bla bla bla', $receiver->getOutput());
        $invoker->undo();
        $this->assertSame('', $receiver->getOutput());
    }
}
