<?php declare(strict_types=1);

namespace Behavioral\TemplateMethod\Worker;

abstract class Worker
{
    /**
     * @var string[]
     */
    private array $thingsToDo = [];

    final public function doYourThing(): void
    {
        $this->thingsToDo[] = $this->work();
        $this->thingsToDo[] = $this->family();
        $this->thingsToDo[] = $this->sleep();
        $this->thingsToDo[] = $this->getSalary();
    }

    abstract protected function work(): string;

    abstract protected function getSalary(): int;

    protected function family(): string
    {
        return 'Familyyy';
    }

    protected function sleep(): string
    {
        return 'Sleeep';
    }

    /**
     * @return string[]
     */
    public function getThingsToDo(): array
    {
        return $this->thingsToDo;
    }
}
