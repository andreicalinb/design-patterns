<?php declare(strict_types=1);

namespace Behavioral\TemplateMethod\Worker;

class FireFighter extends Worker
{
    protected function work(): string
    {
        return 'Dieee fireee';
    }

    protected function getSalary(): int
    {
        return 2500;
    }
}
