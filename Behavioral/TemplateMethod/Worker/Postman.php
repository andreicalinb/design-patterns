<?php declare(strict_types=1);

namespace Behavioral\TemplateMethod\Worker;

class Postman extends Worker
{
    protected function work(): string
    {
        return 'Delivering things';
    }

    protected function getSalary(): int
    {
        return 2000;
    }
}
