<?php declare(strict_types=1);

namespace Behavioral\TemplateMethod;

use Behavioral\TemplateMethod\Worker\FireFighter;
use Behavioral\TemplateMethod\Worker\Postman;
use TestCase;

class TemplateMethodTest extends TestCase
{
    public function test(): void
    {
        $fireFighter = new FireFighter();
        $fireFighter->doYourThing();
        $this->assertSame(['Dieee fireee', 'Familyyy', 'Sleeep', 2500], $fireFighter->getThingsToDo());

        $postman = new Postman();
        $postman->doYourThing();
        $this->assertSame(['Delivering things', 'Familyyy', 'Sleeep', 2000], $postman->getThingsToDo());
    }
}
