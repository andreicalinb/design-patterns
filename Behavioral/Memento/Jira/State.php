<?php declare(strict_types=1);

namespace Behavioral\Memento\Jira;

use InvalidArgumentException;

class State implements \Stringable
{
    const STATE_OPENED = 'opened';
    const STATE_IN_PROGRESS = 'in-progress';
    const STATE_DONE = 'done';

    private string $state;

    /**
     * @var string[]
     */
    private static array $validStates = [
        self::STATE_OPENED,
        self::STATE_IN_PROGRESS,
        self::STATE_DONE,
    ];

    public function __construct(string $state)
    {
        self::ensureIsValidState($state);

        $this->state = $state;
    }

    private static function ensureIsValidState(string $state): void
    {
        if (!in_array($state, self::$validStates)) {
            throw new InvalidArgumentException('Invalid state given');
        }
    }

    public function __toString(): string
    {
        return $this->state;
    }
}
