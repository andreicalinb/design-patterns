<?php declare(strict_types=1);

namespace Behavioral\Memento\Jira;

class Ticket
{
    private State $currentState;

    public function __construct()
    {
        $this->currentState = new State(State::STATE_OPENED);
    }

    public function inProgress(): void
    {
        $this->currentState = new State(State::STATE_IN_PROGRESS);
    }

    public function done(): void
    {
        $this->currentState = new State(State::STATE_DONE);
    }

    public function saveToMemento(): Memento
    {
        return new Memento(clone $this->currentState);
    }

    public function restoreFromMemento(Memento $memento): void
    {
        $this->currentState = $memento->getState();
    }

    public function getState(): State
    {
        return $this->currentState;
    }
}
