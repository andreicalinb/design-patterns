<?php declare(strict_types=1);

namespace Behavioral\Memento;

use Behavioral\Memento\Jira\State;
use Behavioral\Memento\Jira\Ticket;
use TestCase;

class MementoTest extends TestCase
{
    public function test(): void
    {
        $ticket = new Ticket();
        $this->assertSame(State::STATE_OPENED, (string) $ticket->getState());
        $memento = $ticket->saveToMemento();

        $ticket->inProgress();
        $this->assertSame(State::STATE_IN_PROGRESS, (string) $ticket->getState());

        $ticket->restoreFromMemento($memento);
        $this->assertSame(State::STATE_OPENED, (string) $ticket->getState());

        $ticket->done();
        $this->assertSame(State::STATE_DONE, (string) $ticket->getState());
    }
}
