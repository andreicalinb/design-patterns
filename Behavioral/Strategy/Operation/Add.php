<?php declare(strict_types=1);

namespace Behavioral\Strategy\Operation;

class Add implements OperationInterface
{
    public function compute(int $a, int $b): int
    {
        return $a + $b;
    }
}
