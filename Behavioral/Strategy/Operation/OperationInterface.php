<?php declare(strict_types=1);

namespace Behavioral\Strategy\Operation;

interface OperationInterface
{
    public function compute(int $a, int $b): int;
}
