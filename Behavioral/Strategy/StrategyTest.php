<?php declare(strict_types=1);

namespace Behavioral\Strategy;

use Behavioral\Strategy\Operation\Add;
use Behavioral\Strategy\Operation\OperationInterface;
use Behavioral\Strategy\Operation\Substract;
use TestCase;

class StrategyTest extends TestCase
{
    public function test(): void
    {
        $add = new Add();
        $this->assertClass(OperationInterface::class, $add);
        $this->assertSame(3, $add->compute(2, 1));

        $substract = new Substract();
        $this->assertClass(OperationInterface::class, $substract);
        $this->assertSame(1, $substract->compute(2, 1));
    }
}
