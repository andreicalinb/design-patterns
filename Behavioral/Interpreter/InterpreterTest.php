<?php declare(strict_types=1);

namespace Behavioral\Interpreter;

use Behavioral\Interpreter\Expression\AndExpression;
use Behavioral\Interpreter\Expression\Context;
use Behavioral\Interpreter\Expression\OrExpression;
use Behavioral\Interpreter\Expression\VariableExpression;

class InterpreterTest extends \TestCase
{
    public function test(): void
    {
        $context = new Context();
        $shaormaVariableExpression = new VariableExpression('shaorma');
        $context->assign($shaormaVariableExpression, true);
        $garlicSauceVariableExpression = new VariableExpression('garlic-sauce');
        $context->assign($garlicSauceVariableExpression, false);
        $frenchFriesVariableExpression = new VariableExpression('french-fries');
        $context->assign($frenchFriesVariableExpression, false);

        $orExpression = new OrExpression($shaormaVariableExpression, $garlicSauceVariableExpression);
        $this->assertTrue($orExpression->interpret($context));

        $orExpression = new OrExpression($shaormaVariableExpression, $frenchFriesVariableExpression);
        $this->assertTrue($orExpression->interpret($context));

        $orExpression = new OrExpression($garlicSauceVariableExpression, $frenchFriesVariableExpression);
        $this->assertTrue(!$orExpression->interpret($context));

        $andExpression = new AndExpression($shaormaVariableExpression, $garlicSauceVariableExpression);
        $this->assertTrue(!$andExpression->interpret($context));

        $andExpression = new OrExpression($shaormaVariableExpression, $frenchFriesVariableExpression);
        $this->assertTrue($andExpression->interpret($context));

        $andExpression = new OrExpression($garlicSauceVariableExpression, $frenchFriesVariableExpression);
        $this->assertTrue(!$andExpression->interpret($context));
    }
}
