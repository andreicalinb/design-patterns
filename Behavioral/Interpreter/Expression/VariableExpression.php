<?php declare(strict_types=1);

namespace Behavioral\Interpreter\Expression;

class VariableExpression extends AbstractExpression
{
    public function __construct(private string $name)
    {
    }

    public function interpret(Context $context): bool
    {
        return $context->lookUp($this->name);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
