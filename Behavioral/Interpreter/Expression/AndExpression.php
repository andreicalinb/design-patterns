<?php declare(strict_types=1);

namespace Behavioral\Interpreter\Expression;

class AndExpression extends AbstractExpression
{
    public function __construct(private AbstractExpression $first, private AbstractExpression $second)
    {
    }

    public function interpret(Context $context): bool
    {
        return $this->first->interpret($context) && $this->second->interpret($context);
    }
}
