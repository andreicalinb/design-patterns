<?php declare(strict_types=1);

namespace Behavioral\Interpreter\Expression;

use Exception;

class Context
{
    private array $poolVariables;

    public function lookUp(string $name): bool
    {
        if (!key_exists($name, $this->poolVariables)) {
            throw new Exception("Variable does not exist : $name");
        }

        return $this->poolVariables[$name];
    }

    public function assign(VariableExpression $variable, bool $val): void
    {
        $this->poolVariables[$variable->getName()] = $val;
    }
}
