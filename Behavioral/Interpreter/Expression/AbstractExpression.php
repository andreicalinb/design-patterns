<?php declare(strict_types=1);

namespace Behavioral\Interpreter\Expression;

abstract class AbstractExpression
{
    abstract public function interpret(Context $context): bool;
}
