<?php declare(strict_types=1);

namespace Structural\Decorator;

use Structural\Decorator\Shaorma\ShaormaInterface;
use Structural\Decorator\Shaorma\ShaormaWithEverything;
use Structural\Decorator\Shaorma\ShaormaWithExtraSauce;
use TestCase;

class DecoratorTest extends TestCase
{
    public function test(): void
    {
        $shaorma = new ShaormaWithEverything();
        $this->assertClass(ShaormaInterface::class, $shaorma);
        $this->assertSame(24.99, $shaorma->calculatePrice());
        $this->assertSame('Shaorma corecta', $shaorma->getDescription());

        $shaormaWithExtraSauce = new ShaormaWithExtraSauce($shaorma);
        $this->assertClass(ShaormaInterface::class, $shaormaWithExtraSauce);
        $this->assertSame(27.49, $shaormaWithExtraSauce->calculatePrice());
        $this->assertSame('Shaorma corecta with extra sauce', $shaormaWithExtraSauce->getDescription());
    }
}
