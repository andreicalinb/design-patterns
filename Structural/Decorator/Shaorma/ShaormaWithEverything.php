<?php declare(strict_types=1);

namespace Structural\Decorator\Shaorma;

class ShaormaWithEverything implements ShaormaInterface
{
    public function calculatePrice(): float
    {
        return 24.99;
    }

    public function getDescription(): string
    {
        return 'Shaorma corecta';
    }
}
