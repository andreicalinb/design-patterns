<?php declare(strict_types=1);

namespace Structural\Decorator\Shaorma;

abstract class ShaormaDecorator implements ShaormaInterface
{
    public function __construct(protected ShaormaInterface $shaorma)
    {
    }
}
