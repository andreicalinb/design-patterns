<?php declare(strict_types=1);

namespace Structural\Decorator\Shaorma;

class ShaormaWithExtraSauce extends ShaormaDecorator
{
    private const PRICE = 2.5;

    public function calculatePrice(): float
    {
        return $this->shaorma->calculatePrice() + self::PRICE;
    }

    public function getDescription(): string
    {
        return $this->shaorma->getDescription() . ' with extra sauce';
    }
}
