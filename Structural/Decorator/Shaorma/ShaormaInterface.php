<?php declare(strict_types=1);

namespace Structural\Decorator\Shaorma;

interface ShaormaInterface
{
    public function calculatePrice(): float;

    public function getDescription(): string;
}
