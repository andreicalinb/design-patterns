<?php declare(strict_types=1);

namespace Structural\Proxy\Orange;

interface OrangeInterface
{
    public function storeUsedMinutes(int $usedMinutes): void;

    public function getUsedMinutes(): int;
}
