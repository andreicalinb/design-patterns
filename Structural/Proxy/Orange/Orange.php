<?php declare(strict_types=1);

namespace Structural\Proxy\Orange;

class Orange implements OrangeInterface
{
    /**
     * @var int[]
     */
    private array $usedMinutes = [];

    public function storeUsedMinutes(int $usedMinutes): void
    {
        $this->usedMinutes[] = $usedMinutes;
    }

    public function getUsedMinutes(): int
    {
        return array_sum($this->usedMinutes);
    }
}
