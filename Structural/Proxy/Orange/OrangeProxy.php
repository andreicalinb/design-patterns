<?php declare(strict_types=1);

namespace Structural\Proxy\Orange;

class OrangeProxy extends Orange implements OrangeInterface
{
    private ?int $usedMinutesBalance = null;

    public function getUsedMinutes(): int
    {
        if ($this->usedMinutesBalance === null) {
            $this->usedMinutesBalance = parent::getUsedMinutes();
        }

        return $this->usedMinutesBalance;
    }
}
