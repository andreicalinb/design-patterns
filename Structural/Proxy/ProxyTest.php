<?php declare(strict_types=1);

namespace Structural\Proxy;

use Structural\Proxy\Orange\OrangeProxy;
use TestCase;

class ProxyTest extends TestCase
{
    public function test(): void
    {
        $orange = new OrangeProxy();
        
        $orange->storeUsedMinutes(7);
        $this->assertSame(7, $orange->getUsedMinutes());

        $orange->storeUsedMinutes(8);
        $this->assertSame(7, $orange->getUsedMinutes());
    }
}
