<?php declare(strict_types=1);

namespace Structural\DependencyInjection\Laptop;

class Laptop
{
    public function __construct(private Battery $battery)
    {
    }

    public function getBatteryInformation(): string
    {
        return sprintf('Capacity of %s by %s model', $this->battery->getCapacity(), $this->battery->getModel());
    }
}
