<?php declare(strict_types=1);

namespace Structural\DependencyInjection\Laptop;

class Battery
{
    public function __construct(
        private int $capacity,
        private string $model,
    ) {
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function getModel(): string
    {
        return $this->model;
    }
}
