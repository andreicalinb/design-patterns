<?php declare(strict_types=1);

namespace Structural\DependencyInjection;

use Structural\DependencyInjection\Laptop\Battery;
use Structural\DependencyInjection\Laptop\Laptop;
use TestCase;

class DependencyInjectionTest extends TestCase
{
    public function test(): void
    {
        $battery = new Battery(5000, 'Samsung A123');
        $laptop = new Laptop($battery);
        $this->assertSame('Capacity of 5000 by Samsung A123 model', $laptop->getBatteryInformation());
    }
}
