<?php declare(strict_types=1);

namespace Structural\Facade;

use Structural\Facade\TownHall\AnafService;
use Structural\Facade\TownHall\GetInQueueService;
use Structural\Facade\TownHall\ParkingService;
use Structural\Facade\TownHall\TownHallFacade;
use TestCase;

class FacadeTest extends TestCase
{
    public function test(): void
    {
        $anafService = new AnafService();
        $parkingService = new ParkingService();
        $getInQueueService = new GetInQueueService();
        $townHallFacade = new TownHallFacade($anafService, $parkingService, $getInQueueService);

        $this->assertTrue($townHallFacade->payAnafTaxes());
        $this->assertTrue($townHallFacade->payYearlyParkingTax());
    }
}
