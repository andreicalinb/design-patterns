<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

class TownHallFacade
{
    public function __construct(private AnafServiceInterface $anafService, private ParkingServiceInterface $parkingService, private GetInQueueServiceInterface $getInQueueService)
    {
    }

    public function payAnafTaxes(): bool
    {
        $this->anafService->prepare($this->getInQueueService);

        return $this->anafService->payTaxes();
    }

    public function payYearlyParkingTax(): bool
    {
        $this->parkingService->prepare($this->getInQueueService);

        return $this->parkingService->payYearlyTax();
    }
}
