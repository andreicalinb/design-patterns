<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

interface GetInQueueServiceInterface
{
    public function enqueue(): void;
}
