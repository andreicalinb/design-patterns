<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

interface ParkingServiceInterface
{
    public function prepare(GetInQueueServiceInterface $os): void;

    public function payYearlyTax(): bool;
}
