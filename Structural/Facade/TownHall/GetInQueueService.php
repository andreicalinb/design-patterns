<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

class GetInQueueService implements GetInQueueServiceInterface
{
    public function enqueue(): void
    {
    }
}
