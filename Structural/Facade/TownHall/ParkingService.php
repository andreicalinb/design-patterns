<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

class ParkingService implements ParkingServiceInterface
{
    public function prepare(GetInQueueServiceInterface $os): void
    {
    }

    public function payYearlyTax(): bool
    {
        return true;
    }
}
