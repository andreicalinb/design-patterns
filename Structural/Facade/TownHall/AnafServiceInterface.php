<?php declare(strict_types=1);

namespace Structural\Facade\TownHall;

interface AnafServiceInterface
{
    public function prepare(GetInQueueServiceInterface $os): void;

    public function payTaxes(): bool;
}
