<?php declare(strict_types=1);

namespace Structural\Flyweight;

use Structural\Flyweight\MamaManu\MamaManu;
use TestCase;

class FlyweightTest extends TestCase
{
    public function test(): void
    {
        $mamaManu = new MamaManu();
        $this->assertSame($mamaManu->count(), 0);

        $morningCasPane = $mamaManu->get('morning-cas-pane');
        $this->assertSame($morningCasPane, $mamaManu->get('morning-cas-pane'));
        $this->assertSame($mamaManu->count(), 1);

        $noonCasPane = $mamaManu->get('noon-cas-pane');
        $this->assertSame($noonCasPane, $mamaManu->get('noon-cas-pane'));
        $this->assertSame($mamaManu->count(), 2);

        $eveningCasPane = $mamaManu->get('evening-cas-pane');
        $this->assertSame($eveningCasPane, $mamaManu->get('evening-cas-pane'));
        $this->assertSame($mamaManu->count(), 3);
    }
}
