<?php declare(strict_types=1);

namespace Structural\Flyweight\MamaManu;

class CasPane implements ProductOrderedInterface
{
    public function __construct(private string $name)
    {
    }

    public static function withName(string $name): ProductOrderedInterface
    {
        return new self($name);
    }
}
