<?php declare(strict_types=1);

namespace Structural\Flyweight\MamaManu;

interface ProductOrderedInterface
{
    public static function withName(string $name): ProductOrderedInterface;
}
