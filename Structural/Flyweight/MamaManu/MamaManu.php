<?php declare(strict_types=1);

namespace Structural\Flyweight\MamaManu;

use Countable;

class MamaManu implements Countable
{
    /**
     * @var ProductOrderedInterface[]
     */
    private array $productsOrdered = [];

    public function get(string $name): ProductOrderedInterface
    {
        if (!isset($this->productsOrdered[$name])) {
            $this->productsOrdered[$name] = $this->create($name);
        }

        return $this->productsOrdered[$name];
    }

    private function create(string $name): ProductOrderedInterface
    {
        return CasPane::withName($name);
    }

    public function count(): int
    {
        return count($this->productsOrdered);
    }
}
