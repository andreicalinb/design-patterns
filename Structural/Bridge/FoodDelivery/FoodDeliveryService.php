<?php declare(strict_types=1);

namespace Structural\Bridge\FoodDelivery;

use Structural\Bridge\DeliveryMethod\DeliveryMethodInterface;

abstract class FoodDeliveryService
{
    public function __construct(protected DeliveryMethodInterface $implementation)
    {
    }

    public function setImplementation(DeliveryMethodInterface $printer): void
    {
        $this->implementation = $printer;
    }

    abstract public function deliver(): string;
}
