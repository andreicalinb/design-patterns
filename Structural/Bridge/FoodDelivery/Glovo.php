<?php declare(strict_types=1);

namespace Structural\Bridge\FoodDelivery;

class Glovo extends FoodDeliveryService
{
    public function deliver(): string
    {
        return $this->implementation->goGoGo('Delivered by Glovo');
    }
}
