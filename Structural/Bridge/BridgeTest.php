<?php declare(strict_types=1);

namespace Structural\Bridge;

use Structural\Bridge\DeliveryMethod\BikeDeliveryMethod;
use Structural\Bridge\DeliveryMethod\CarDeliveryMethod;
use Structural\Bridge\FoodDelivery\Foodpanda;
use Structural\Bridge\FoodDelivery\Glovo;
use TestCase;

class BridgeTest extends TestCase
{
    public function test(): void
    {
        $foodpanda = new Foodpanda(new CarDeliveryMethod());
        $this->assertSame('Delivered by Foodpanda', $foodpanda->deliver());

        $glovo = new Glovo(new BikeDeliveryMethod());
        $this->assertSame('Delivered by Glovo', $glovo->deliver());
    }
}
