<?php declare(strict_types=1);

namespace Structural\Bridge\DeliveryMethod;

class CarDeliveryMethod implements DeliveryMethodInterface
{
    public function goGoGo(string $text): string
    {
        return $text;
    }
}
