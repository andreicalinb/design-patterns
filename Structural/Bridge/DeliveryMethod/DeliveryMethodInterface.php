<?php declare(strict_types=1);

namespace Structural\Bridge\DeliveryMethod;

interface DeliveryMethodInterface
{
    public function goGoGo(string $text): string;
}
