<?php declare(strict_types=1);

namespace Structural\Bridge\DeliveryMethod;

class BikeDeliveryMethod implements DeliveryMethodInterface
{
    public function goGoGo(string $text): string
    {
        return $text;
    }
}
