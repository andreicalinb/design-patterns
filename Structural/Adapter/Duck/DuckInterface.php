<?php declare(strict_types=1);

namespace Structural\Adapter\Duck;

interface DuckInterface
{
    public function squeak(): string;
}
