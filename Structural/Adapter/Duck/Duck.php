<?php declare(strict_types=1);

namespace Structural\Adapter\Duck;

class Duck implements DuckInterface
{
    public function squeak(): string
    {
        return 'Quack';
    }
}
