<?php declare(strict_types=1);

namespace Structural\Adapter\Duck;

class ToyDuck implements ToyDuckInterface
{
    public function squeeze(): string
    {
        return 'Ihiiii';
    }
}
