<?php declare(strict_types=1);

namespace Structural\Adapter\Duck;

interface ToyDuckInterface
{
    public function squeeze(): string;
}
