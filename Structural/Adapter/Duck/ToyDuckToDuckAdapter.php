<?php declare(strict_types=1);

namespace Structural\Adapter\Duck;

class ToyDuckToDuckAdapter implements DuckInterface
{
    public function __construct(protected ToyDuckInterface $toyDuck)
    {
    }

    public function squeak(): string
    {
        return $this->toyDuck->squeeze();
    }
}
