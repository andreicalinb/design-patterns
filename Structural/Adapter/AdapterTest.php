<?php declare(strict_types=1);

namespace Structural\Adapter;

use Structural\Adapter\Duck\Duck;
use Structural\Adapter\Duck\DuckInterface;
use Structural\Adapter\Duck\ToyDuck;
use Structural\Adapter\Duck\ToyDuckInterface;
use Structural\Adapter\Duck\ToyDuckToDuckAdapter;
use TestCase;

class AdapterTest extends TestCase
{
    public function test(): void
    {
        $duck = new Duck();
        $this->assertClass(DuckInterface::class, $duck);
        $this->assertSame('Quack', $duck->squeak());

        $toyDuck = new ToyDuck();
        $this->assertClass(ToyDuckInterface::class, $toyDuck);
        $this->assertSame('Ihiiii', $toyDuck->squeeze());

        $toyDuckAdapter = new ToyDuckToDuckAdapter($toyDuck);
        $this->assertClass(DuckInterface::class, $toyDuckAdapter);
        $this->assertSame('Ihiiii', $toyDuckAdapter->squeak());
    }
}
