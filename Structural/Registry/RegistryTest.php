<?php declare(strict_types=1);

namespace Structural\Registry;

use InvalidArgumentException;
use Structural\Registry\Services\KFC;
use Structural\Registry\Services\MC;
use Structural\Registry\Services\Registry;
use TestCase;

class RegistryTest extends TestCase
{
    public function test(): void
    {
        try {
            Registry::get(Registry::KEY_KFC);
            echo 'You should not see me';
        } catch (InvalidArgumentException) {
        }

        $kfc = new KFC();
        $mc = new MC();
        Registry::set(Registry::KEY_KFC, $kfc);
        $this->assertSame($kfc, Registry::get(Registry::KEY_KFC));

        try {
            Registry::set('mc', $mc);
            echo 'You should not see me';
        } catch (InvalidArgumentException) {
        }
    }
}
