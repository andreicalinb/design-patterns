<?php declare(strict_types=1);

namespace Structural\Registry\Services;

use InvalidArgumentException;

abstract class Registry
{
    const KEY_KFC = 'kfc';

    private static array $services = [];

    private static array $allowedKeys = [
        self::KEY_KFC,
    ];

    public static function set(string $key, object $value): void
    {
        if (!in_array($key, self::$allowedKeys)) {
            throw new InvalidArgumentException('Not allowed service key given');
        }

        self::$services[$key] = $value;
    }

    public static function get(string $key): object
    {
        if (!in_array($key, self::$allowedKeys) || !isset(self::$services[$key])) {
            throw new InvalidArgumentException('Not allowed service key given');
        }

        return self::$services[$key];
    }
}
