<?php declare(strict_types=1);

namespace Structural\FluentInterface;

use Structural\FluentInterface\Sql\Sql;
use TestCase;

class FluentInterfaceTest extends TestCase
{
    public function test(): void
    {
        $sql = new Sql();
        $query = $sql->select(['aaa', 'bbb'])
            ->from('ccc', 'ddd')
            ->where('ddd.eee = ?');

        $this->assertSame('SELECT aaa, bbb FROM ccc AS ddd WHERE ddd.eee = ?', (string) $query);
    }
}
