<?php declare(strict_types=1);

namespace Structural\Composite;

use Structural\Composite\Company\BackendDeveloper;
use Structural\Composite\Company\Company;
use Structural\Composite\Company\FrontendDeveloper;
use TestCase;

class CompositeTest extends TestCase
{
    public function test(): void
    {
        $company = new Company();
        $company->addEmployee(new FrontendDeveloper('React'));
        $company->addEmployee(new BackendDeveloper('Symfony'));
        $this->assertSame("React\nSymfony\n", $company->work());
    }
}
