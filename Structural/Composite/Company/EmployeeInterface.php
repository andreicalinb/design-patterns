<?php declare(strict_types=1);

namespace Structural\Composite\Company;

interface EmployeeInterface
{
    public function work(): string;
}
