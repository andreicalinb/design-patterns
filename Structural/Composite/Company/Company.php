<?php declare(strict_types=1);

namespace Structural\Composite\Company;

class Company implements EmployeeInterface
{
    /**
     * @var EmployeeInterface[]
     */
    private array $employees;

    public function work(): string
    {
        $works = '';
        foreach ($this->employees as $employee) {
            $works .= $employee->work() . "\n";
        }

        return $works;
    }

    public function addEmployee(EmployeeInterface $employee): void
    {
        $this->employees[] = $employee;
    }
}
