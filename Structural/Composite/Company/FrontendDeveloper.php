<?php declare(strict_types=1);

namespace Structural\Composite\Company;

class FrontendDeveloper implements EmployeeInterface
{
    public function __construct(private string $text)
    {
    }

    public function work(): string
    {
        return $this->text;
    }
}
