<?php declare(strict_types=1);

namespace Structural\DataMapper\Books;

use InvalidArgumentException;

class BookMapper
{
    public function __construct(private StorageAdapter $adapter)
    {
    }

    public function findById(int $id): Book
    {
        $result = $this->adapter->find($id);

        if ($result === null) {
            throw new InvalidArgumentException(sprintf('Book with id %d not found', $id));
        }

        return $this->mapRowToBook($result);
    }

    private function mapRowToBook(array $row): Book
    {
        return Book::fromState($row);
    }
}
