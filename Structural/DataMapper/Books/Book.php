<?php declare(strict_types=1);

namespace Structural\DataMapper\Books;

class Book
{
    public function __construct(private string $title, private string $author)
    {
    }

    public static function fromState(array $state): Book
    {
        return new self($state['title'], $state['author']);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}
