<?php declare(strict_types=1);

namespace Structural\DataMapper;

use InvalidArgumentException;
use Structural\DataMapper\Books\Book;
use Structural\DataMapper\Books\BookMapper;
use Structural\DataMapper\Books\StorageAdapter;
use TestCase;

class DataMapperTest extends TestCase
{
    public function test(): void
    {
        $storage = new StorageAdapter([1 => ['title' => 'Design Patterns', 'author' => 'Erich Gamma']]);
        $bookMapper = new BookMapper($storage);
        $book = $bookMapper->findById(1);
        $this->assertClass(Book::class, $book);

        try {
            $bookMapper->findById(2);
            echo 'You should not see me';
        } catch (InvalidArgumentException) {
        }
    }
}
