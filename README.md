# What

This is the implementation code for the `Design Patterns: Elements of Reusable Object-Oriented Software` book, whose notes can be found [here](https://docs.google.com/document/d/1c7Eo8-L3OXp5TfS1u7G3Zb2-NVHUrJGRGtm0DQzFKiI/edit?usp=sharing).

# Why

The scope was to improve my design pattern skill.
