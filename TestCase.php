<?php declare(strict_types=1);

class TestCase
{
    /**
     * @throws \Exception
     */
    protected function assertClass(string $class, object $object): void
    {
        if (!($object instanceof $class)) {
            throw new \Exception(sprintf('Object class (%s) is not an instance of %s', get_class($object), $class));
        }
    }

    /**
     * @throws \Exception
     */
    protected function assertSame($a, $b): void
    {
        if ($a !== $b) {
            throw new \Exception(sprintf('Variables (%s) (%s) are not the same', (string)$a, (string)$b));
        }
    }

    /**
     * @throws \Exception
     */
    protected function assertNotSame($a, $b): void
    {
        if ($a === $b) {
            throw new \Exception(sprintf('Variables (%s) (%s) are not the same', (string)$a, (string)$b));
        }
    }

    /**
     * @throws \Exception
     */
    protected function assertTrue($a): void
    {
        if ($a !== true) {
            throw new \Exception(sprintf('Variable (%s) is not true', (string)$a));
        }
    }
}
